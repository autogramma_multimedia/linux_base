Манифест для repо -- https://gitlab.com/autogramma_multimedia/linux_base (master)

meta-репозиторий для Yocto -- https://gitlab.com/autogramma_multimedia/meta-autogramma (dunfell)

Ядро -- https://gitlab.com/autogramma_multimedia/linux_kernel (imx_4.14.78_1.0.0_ga_var01)

Собирать по этому гайду: https://variwiki.com/index.php?title=Yocto_Build_Release&release=RELEASE_DUNFELL_V1.0_VAR-SOM-MX6

Небольшие отличие, в том, что в пункте 3 нужно сменить репозиторий с Variscite на Autogramma
$ repo init -u https://gitlab.com/autogramma_multimedia/linux_base.git -b master

------------------------------------------------------------------------------------------------
Подготовка к установке.


Для систем с ubuntu 16.04LTS Нужно предустановить python3.6:

$ sudo add-apt-repository ppa:fkrull/deadsnakes
$ sudo apt-get update
$ sudo apt-get install python3.6

repo используется старой версии его взять отсюда:
$ sudo curl https://gitlab.com/autogramma_multimedia/repo/-/raw/main/repo_k16?inline=false > /usr/bin/repo
$ chmod a+x /usr/bin/repo


Для всех систем пакеты нужные для установки
$ sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib\
 build-essential chrpath socat cpio python python3 python3-pip python3-pexpect\
 xz-utils debianutils iputils-ping python3-git python3-jinja2 libegl1-mesa\
 libsdl1.2-dev pylint3 xterm

------------------------------------------------------------------------------------------------

Описание содержимого:
linux_base		- репозиторий с этим описанием и манифестом для REPO
linux_kernel		- репозиотрий с fork версией ядра адаптированной для этого проекта
uboot-imx		- репозиторий с fork uboot адаптированный для этого проекта
meta-autogramma         - новый репозиторий с рецептам для сборки образов с помощью yocto
var-fslc-yocto		- рабочая папка ситемы yocto в неё выкачиваются зависимости и в ней идёт сборка(можно менять)

------------------------------------------------------------------------------------------------

Список команд для установки:

1:
$ git config --global user.name "Your Name"
$ git config --global user.email "Your Email"

$ mkdir ~/bin (this step may not be needed if the bin folder already exists)
$ curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
$ chmod a+x ~/bin/repo
$ export PATH=~/bin:$PATH

2:
$ mkdir ./var-fslc-yocto
$ cd ./var-fslc-yocto

3:
repo init -u https://gitlab.com/autogramma_multimedia/linux_base.git -b master
repo sync -j1

4:
./menu.sh

5:(ручной запуск сборки если menu.sh не работает)
MACHINE=ag-bdb-mx6 DISTRO=fslc-framebuffer-ag
setup-environment build_fb
bitbake ag-image-qt5

------------------------------------------------------------------------------------------------

Как коммитить изменения В МАНИФЕСТ:

Сам репозиторий манифеста находится в папке .repo/manifests, в котором можно проводить свои изменения без 
выкачивания отдельно.

-1. УБЕДИТЕСЬ, что вы находитесь на ветке dunfell:
`$ git checkout dunfell`

0. Находимся в состоянии правки манифеста неопределенное время.

1. Добавляем свои изменения с `git add` и закоммичиваем с `git commit`

2. Пушим изменения на сервер:
`$ git push origin dunfell`

3. Конфликты разруливаются как обычно, пример есть начиная с ПУНКТА 5 следующего раздела.

------------------------------------------------------------------------------------------------

Как коммитить изменения в обычные репозитории, НЕ манифест:

-1. Находимся в состоянии правки исходников неопределенное время.

0. Всегда лучше начать с синхронизации исходников. Это можно сделать через menu.sh. Если выдает конфликт,
то это не страшно, вдальнейшим мы их разрулим.

1. Допустим, делается изменения в meta-autogramma. Переходите в её каталог:
`$ cd sources/meta-autogramma`

2. Перед тем, как перейти на какую-то ветку, надо убедиться, что ветки dunfell нет, используя команду:
`$ git branch`

2.1 Если она есть, то надо её удалить:
`$ git branch -D dunfell`
Главное, не потерять свои изменения, если они там есть. 

3. Далее надо создать и перейти на ветку:
`$ git checkout -b dunfell`

3.1 Если после этого момента, вы сделали repo sync, он перекинет вас на хеш-коммит и руками нужно будет возвращаться
на свою ветку.
`$ git checkout dunfell`

4. Теперь вы добавляете свои изменения через git add, после можно коммитить как обычно.

5. Так как у нас нет кодревью сервера, пушить будет немного необычно:
`$ git push autogramma dunfell`

5.1. Возможно ваши изменения уже расходятся с историей на сервере, в данном случае git выдает тиипичную ошибку:
`error: не удалось отправить некоторые ссылки в «https://gitlab.com/Autogramma_BDB/meta-autogramma»`

В данном случае, необходимо выполнить мерж(при условии того, что вы уже сделали repo sync ДО ПУНКТА 3, иначе СМ ПУНКТ 3.1):
`$ git merge autogramma/dunfell`

Если конфликтов нет, мердж пройдет автоматически.
Если конфликты ЕСТЬ, то `git status` подскажет в каких файлах они есть.
Нужно в текстовом редакторе отредактировать эти файлы в конфликтных местах в то состояние, которое войдет в конечный
результат мерджа.

5.2 Конфликтные файлы после правки надо так же добавлять через git add и закоммитить финальный результат мерджа.

5.3. Делаем push снова, как в пункте 5.

6. Вам нужно перед удалением перейти на хешкоммит обратно, можно это сделать с помощью команды:
`$ repo sync`
ИЛИ альтернативным способом:
`$ git checkout autogramma/dunfell` 

7. Лучше не забывать удалять(забыть) свою ветку, дабы не вводить в конфуз себя:
`$ git branch -D dunfell`

8. Проверить можно с `repo sync` и на сайте, ругаться ни на что не должно.
